// Functions
	// Functions are lines/ block of codes that tell our device/ application to perform a certai task when called or invoked


// Function Declarations
/* 
	function functionName() {
		code block(statements)
	};
*/

// a function to print name

	function printName(){
		console.log("My name is Hannah")
	};

// Function invocation
	printName();
	// can be invoke multiple times
	printName();


	// declaredFunction(); is not defined, you cannot invoike the function unless it is declared

// Function declaration vs. expressions

	declaredFunction();

	function declaredFunction(){
		console.log("Hi, I am from declaredFunction()")
	}

	//reassign declared function

	declaredFunction = function(){
		console.log("This is an updated declaredFunction");
	}

	declaredFunction();

	//reassignment of a function expression with const is not possiblke

	// const constantFunc = function(){
	// 	console.log("Initialized with const");
	// };

	// constantFunc();

	// constantFunc = function(){
	// 	console.log("Cannot be assigned");
	// };

	// constantFunc();


// Function scoping

	{
		let localVar = "Armando Perez"
	};

	let globalVar = "Mr. Worldwide";
	console.log(globalVar);

	// Function Scope

	var functionVar = "Joe";
	let functionLet = "Jane";
	const functionConst = "Jake";

	function showNames(){
		var functionVar = "Joe";
		let functionLet = "Jane";
		const functionConst = "Jake";

		console.log(functionVar);
		console.log(functionLet);
		console.log(functionConst);
	};


	showNames();

	// console.log(functionVar);
	// console.log(functionLet);
	// console.log(functionConst);
	// // error

// Nested Function

	function myNewFunction(){
		let name = "Yor";

		function nestedFunction(){
			let nestedName = "Brando";
			console.log(nestedName);
		};
		// console.log(nestedName); // result in error
		nestedFunction();
	};

	myNewFunction();

// Function and Global Scoped Variables
	
	let globalName = "Alex";

	function myNewFunction2(){
		let nameInside = "Marco";
		console.log(globalName);
	};

	myNewFunction2();
	// console.log(nameInside); //error

// alert()
	/*
		Syntax:
		alert("message");
	*/

	// alert("I am here");
	// // alert immediately once the page loads

	// function showSampleAlert(){
	// 	alert("Hello User!");
	// };

	// showSampleAlert();

	// console.log("I will only log in the console when the alert is dismissed.");

	//prompt
		// let samplePrompt = prompt("Enter your name");
		// console.log(typeof samplePrompt);
		// console.log("Hello " + samplePrompt);
		// // if there's no input it will return an empty string
		// // if prompt was canceled, the return will be null

		// function printWelcomeMessage(){
		// 	let firstName = prompt("Enter your first name");
		// 	let lastName = prompt("Enter your last name");

		// 	console.log("Hello " + firstName + " " + lastName + "!");
		// 	console.log("Welcome to my page");
		// }

		// printWelcomeMessage();


	// Function Naming Message
		// Function should be definitive of its task. Usually contains a verb
		
		function getCourses(){
			let courses = ["Science 101", "Trigonometry 103", "Physics 107"]
			console.log(courses);
		};
		getCourses();

		// Avoid generic names

		function get(){
			let name = "Edward";
			console.log(name);
		};
		get();

		// Avoid pointless and inappropriate function names
		function foo(){
			console.log(25%5)
		};

		foo();

		// Name your function is small caps. Follow camelCase when naming variables and function

		function displayCarInfo(){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000")
		};

		displayCarInfo();


